### Projet Web Agency - Site d'Agence Web

## Description
Le projet Web Agency est une reconstitution d'un site d'agence web moderne, développé en utilisant HTML, CSS et Bootstrap. Cette réalisation a pour objectif de démontrer la conception responsive et les fonctionnalités interactives propres à une agence web professionnelle.

## Fonctionnalités principales
- **Design moderne :** Une esthétique contemporaine mettant en avant les dernières tendances en matière de conception web.
- **Responsive :** L'interface s'adapte de manière fluide à différentes tailles d'écrans, offrant une expérience utilisateur optimale.
- **Section de portefeuille :** Présentation des projets réalisés par l'agence avec des détails interactifs.
- **Formulaire de contact :** Les visiteurs peuvent entrer en contact avec l'agence via un formulaire intégré.


## Technologies utilisées
- HTML
- CSS
- Bootstrap

## Installation
1. Clonez le dépôt `git clone https://github.com/votre_utilisateur/web-agency.git`
2. Ouvrez le fichier `index.html` dans votre navigateur web.


## Auteurs
Chems MEZIOU
